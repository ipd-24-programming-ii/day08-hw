package day08_hw_file;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Stream;

public class Address{
	private int buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	private String postCode;
	
	public Address(int buildingNo, String streetName, String cityName, String provinceName, String postCode) {
		this.buildingNo = buildingNo;
		this.streetName = streetName;
		this.cityName = cityName;
		this.provinceName = provinceName;
		this.postCode = postCode;
	}
	
	
	@Override
	public String toString() {
		return "Address [buildingNo=" + buildingNo + ", streetName=" + streetName + ", cityName=" + cityName
				+ ", provinceName=" + provinceName + ", postCode=" + postCode + "]";
	}

	// this is printWriter
	public static void writeToFile(List<Address>addressToWrite) throws IOException{
		try (FileWriter fileWriter = new FileWriter("src/day08_hw_file/resource/address.txt")){
			PrintWriter printWriter = new PrintWriter(fileWriter);
			
			addressToWrite.forEach(element -> printWriter.print(element.toString()+"\n"));
		}catch(IOException e) {
			throw e;
		}
		System.out.println("all address information has been saved in the resource folder successfully!"+"\n");
	}
	
	//this is BufferedReader
	public static void readFromFile() throws IOException{
		try(BufferedReader reader = new BufferedReader(new FileReader("src/day08_hw_file/resource/address.txt"))){
			Stream <String> lines = reader.lines();
			lines.forEach(line -> System.out.println(line));
		}
		catch (IOException e) {
			throw e;
		}
	}
}
