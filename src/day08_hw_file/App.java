package day08_hw_file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args){
		
		List<Address> writeToFile = new  ArrayList<>();
		
		writeToFile.add(new Address(50, "Red", "Montreal","Quebec", "P6S 6J1"));
		writeToFile.add(new Address(30, "Yellow", "Toronto","BC", "Y9T 3G7"));
		writeToFile.add(new Address(10, "Pink", "Vancouver","Ontario", "U6W 9D1"));
		writeToFile.add(new Address(70, "Blue", "Laval","Quebec", "U5J 8G5"));
		writeToFile.add(new Address(400, "Green", "Lachine","Quebec", "K6Q G5U"));
		
		try {
			Address.writeToFile(writeToFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Address.readFromFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
